package calculadoragit;

import java.util.Scanner;

public class CalculadoraGit {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int opc;
        System.out.println("Dime el primer valor");
        int num1 = teclado.nextInt();
        System.out.println("Dime el segundo valor");
        int num2 = teclado.nextInt();
        do {
            System.out.println("1.SUMAR\n2.RESTAR\n3.MULTIPLICAR\n4.DIVIDIR\n0.SALIR\nQue desea hacer");
            opc = teclado.nextInt();
            switch (opc) {
                case 1:
                    System.out.println("RESULTADO DE LA SUMA: " + (num1 + num2));
                    break;
                case 2:
                    System.out.println("RESULTADO DE LA RESTA: " + (num1 - num2));
                    break;
                case 3:
                    System.out.println("RESULTADO DEL PRODUCTO: " + (num1 * num2));
                    break;
                case 4:
                    System.out.println("RESULTADO DE LA DIVISIÓN: " + (num1 / num2));
                    break;
                default:
                    System.out.println("OPCIÓN NO VALIDA");
                    break;
                case 0:
                    System.out.println("FIN");
            }

        } while (opc != 0);
    }

}
